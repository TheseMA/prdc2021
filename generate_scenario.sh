#!/bin/bash

if [ $# -ne 3 ]
then
    echo "need number of node, number of attacker profiles and scenario name"
    exit 1
fi

mkdir -p generated_sc

make all

./bin/parameters_generator_node.x $1 $2 generated_sc/param_$3
./bin/parameters_to_rewards_and_costs.x generated_sc/param_$3  generated_sc/$3


