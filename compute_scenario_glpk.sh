#!/bin/bash

if [ $# -eq 0 ]
then
    echo "need a scenario in arg"
    exit 1
fi

mkdir -p ./results
mkdir -p ./results/$1

make all

{ time timeout 6000 ./bin/solver_convert.x $1 g >> results/$1_res_glpk.sol  ; } 2>> results/$1/dump

echo "solution write in ./results/"$1"_res_glpk.sol"

