#!/bin/bash

if [ $# -ne 1 ]
then
    echo "need number max to compute time "
    exit 1
fi

mkdir -p generated_sc
mkdir -p results
mkdir -p bin

make all

for i in $(seq 1 $1)
do
    
    mkdir -p generated_sc/scenario$i
    rm -f generated_sc/scenario$i/dump

    for j in $(seq 1 2)
    do

    name=scenario$i\_$j

    mkdir -p ./results/generated_sc/scenario"$i"/
    
	./bin/parameters_generator_node.x $i $i generated_sc/scenario$i/param_$name
	./bin/parameters_to_rewards_and_costs.x generated_sc/scenario$i/param_$name  generated_sc/scenario$i/$name	
	./bin/solver_convert.x generated_sc/scenario$i/$name c >> generated_sc/scenario$i/trace
	
	echo "read ./test.lp" > ./generated_sc/scenario$i/res.cplex
	echo "opt" >> ./generated_sc/scenario$i/res.cplex
	echo "write ./generated_sc/scenario"$i"/res.sol" >> ./generated_sc/scenario$i/res.cplex
	echo "quit" >> ./generated_sc/scenario$i/res.cplex
	
	cplex -f ./generated_sc/scenario$i/res.cplex >> "./generated_sc/scenario"$i"/dump"
	
	echo "Trace write in ./generated_sc/scenario"$i"/trace"
	echo "solution write in ./generated_sc/scenario"$i"/res.sol -"
	
	rm cplex.log

	sleep 1
    done

done

./bin/plot_cplex.x $1
