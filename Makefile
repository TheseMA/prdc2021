GCC=gcc

#FLAGS=-Wall -g -D DEBUG
DST = bin/
SRC = src/
FLAGS= -Wpedantic #-D VERBOSE
GCCFLAGS= 
CPPFLAGS= 
LIBS= -lglpk -lz -lltdl -lm

# User definitions must be here
EXEC1 = $(DST)plot_glpk.x
EXEC2 = $(DST)parameters_to_rewards_and_costs.x
EXEC3 = $(DST)parameters_generator_node.x
EXEC4 = $(DST)solver_convert.x
EXEC5 = $(DST)plot_cplex.x

INCS = 

SOURCES1 = $(SRC)plot_glpk.c
SOURCES2 = $(SRC)parameters_to_rewards_and_costs.c
SOURCES3 = $(SRC)parameters_generator_node.c
SOURCES4 = $(SRC)solver_convert.c
SOURCES5 = $(SRC)plot_cplex.c

OBJS1 = $(SOURCES1:.c=.o)
OBJS2 = $(SOURCES2:.c=.o)
OBJS3 = $(SOURCES3:.c=.o)
OBJS4 = $(SOURCES4:.c=.o)
OBJS5 = $(SOURCES5:.c=.o)

.phony: all clean depend $(EXEC1) $(EXEC2) $(EXEC3) $(EXEC4) $(EXEC5) 

# Building the world
all: $(EXEC1) $(EXEC2) $(EXEC3) $(EXEC4) $(EXEC5) 

$(EXEC1): $(INCS) $(OBJS1) 
	 $(GCC) $(GCCFLAGS) $(OBJS1) $(LIBS) -o $(EXEC1) 

$(EXEC2): $(INCS) $(OBJS2) 
	 $(GCC) $(GCCFLAGS) $(OBJS2) $(LIBS) -o $(EXEC2)

$(EXEC3): $(INCS) $(OBJS3)
	 $(GCC) $(GCCFLAGS) $(OBJS3) $(LIBS) -o $(EXEC3)

$(EXEC4): $(INCS) $(OBJS4)
	 $(GCC) $(GCCFLAGS) $(OBJS4) $(LIBS) -o $(EXEC4)

$(EXEC5): $(INCS) $(OBJS5)
	 $(GCC) $(GCCFLAGS) $(OBJS5) $(LIBS) -o $(EXEC5)

.SUFFIXES:
.SUFFIXES: .c .cc .o

.cc.o:
	$(GPP) $(FLAGS) -c $<
.c.o:
	$(GCC) $(FLAGS) -c $< 
	@mv *.o $(SRC)
	@mkdir -p  $(DST)

# Clean up
clean:
	rm -f *~ .*~ \#*\# $(SRC)*.o 
	rm -f $(EXEC1) $(EXEC2) $(EXEC3) $(EXEC4) $(EXEC5)
	rm -fr ./results ./bin ./generated_sc

# Dependencies
depend: 
	touch .depend
	$(GCC) -M $(CPPFLAGS) $(SOURCES1) > .depend
	$(GCC) -M $(CPPFLAGS) $(SOURCES2) >> .depend
	$(GCC) -M $(CPPFLAGS) $(SOURCES3) >> .depend
	$(GCC) -M $(CPPFLAGS) $(SOURCES4) >> .depend
	$(GCC) -M $(CPPFLAGS) $(SOURCES5) >> .depend

-include .depend

