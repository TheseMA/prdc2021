#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glpk.h>
#include <float.h>
#include <limits.h>

#define SIZE 100
#define SIZE_MTD 3

unsigned long cpt_ne = 0;

int *cons;
int *var;

double *val;

int ne = 1;

//              pf  n_d  md n_a  ma
double Rpnmnm [SIZE][SIZE][SIZE_MTD][SIZE][SIZE_MTD];
double Cpnmnm [SIZE][SIZE][SIZE_MTD][SIZE][SIZE_MTD];
double Rhpnmnm[SIZE][SIZE][SIZE_MTD][SIZE][SIZE_MTD];
double Chpnmnm[SIZE][SIZE][SIZE_MTD][SIZE][SIZE_MTD];

//              pf  n_d  n_a
double Rpndnd [SIZE][SIZE][SIZE];
double Cpndnd [SIZE][SIZE][SIZE];
double Rhpndnd[SIZE][SIZE][SIZE];
double Chpndnd[SIZE][SIZE][SIZE];

//             pf   n_d  md n_a 
double Rpnmnd [SIZE][SIZE][SIZE_MTD][SIZE];
double Cpnmnd [SIZE][SIZE][SIZE_MTD][SIZE];
double Rhpnmnd[SIZE][SIZE][SIZE_MTD][SIZE];
double Chpnmnd[SIZE][SIZE][SIZE_MTD][SIZE];

//              pf  n_d  n_a  ma
double Rpndnm [SIZE][SIZE][SIZE][SIZE_MTD];
double Cpndnm [SIZE][SIZE][SIZE][SIZE_MTD];
double Rhpndnm[SIZE][SIZE][SIZE][SIZE_MTD];
double Chpndnm[SIZE][SIZE][SIZE][SIZE_MTD];


unsigned long var_number = 0;

int row_znnmdp = 0;
int one_row = 0;

double profile_proba[10000];

/* Create the constraints names  */
void constraints_name_creator (char *name , int cons_n, int node_n, int mtd_n, int profil_n, int ids) {
  int cpt = 0;

  name[cpt++] = 'c';
  name[cpt++] = cons_n/10 + '0';
  name[cpt++] = cons_n%10 + '0';

  if (profil_n >= 0) {
    name[cpt++] = 'p';
    name[cpt++] = (profil_n/10000) % 10 + '0';
    name[cpt++] = (profil_n/1000) % 10 + '0';
    name[cpt++] = (profil_n/100) % 10 + '0';
    name[cpt++] = (profil_n/10) % 10 + '0';
    name[cpt++] = profil_n%10 + '0';    
  }

  if (node_n >= 0) {
    name[cpt++] = 'n';
    name[cpt++] = (node_n/10000) % 10 + '0';
    name[cpt++] = (node_n/1000) % 10 + '0';
    name[cpt++] = (node_n/100) % 10 + '0';
    name[cpt++] = (node_n/10) % 10 + '0';
    name[cpt++] = node_n%10 + '0';
  }

  if (mtd_n >= 0) {  
    name[cpt++] = 'm';
    name[cpt++] = (mtd_n/10000) % 10 + '0';
    name[cpt++] = (mtd_n/1000) % 10 + '0';
    name[cpt++] = (mtd_n/100) % 10 + '0';
    name[cpt++] = (mtd_n/10) % 10 + '0';
    name[cpt++] = mtd_n%10 + '0';
  }

  if (ids > 0) {
    name[cpt++] = 'd';
  }
  
  name[cpt] = '\0';
  return;
}

void create_constraints (glp_prob *mip, unsigned long *cpt, int node_n, int mtd_n, int profil_n, int ids, double lb, double ub, int cons_type, int cons_nb, int* cons_number) {
  char name[5000];

  cons_number[0] = glp_add_rows(mip, 1);
  constraints_name_creator(name, cons_nb, node_n, mtd_n, profil_n, ids);
  glp_set_row_name (mip, cpt[0] , name);
  glp_set_row_bnds (mip, cpt[0], cons_type, lb, ub);
  cpt[0]++;

#ifdef VERBOSE
  printf("%s\n", name);
#endif 
  
  return; 
}


/* Add all the constraints to the problem */
void add_constraints (glp_prob *mip, int *cons_numbers, int profil_number, int node_number, double  M, int row_zij, int mtd_max, int *mtd_node_number) {
  unsigned long cpt = 1;

#ifdef VERBOSE
  printf("%d\n", cons_numbers[0]);
#endif 
  
  
  //glp_add_rows(mip, cons_numbers)
  for (int m = 0 ; m < profil_number ; m ++) {
    // cons 1 DOBSS : there are p cons 1--  == 1 
    create_constraints (mip, &cpt, -1, -1, m, -1, 1.0, 1.0, GLP_FX, 1, cons_numbers);

    // cons 2 DOBSS : there are p cons 2 --  <= 1
    for (int n = 0 ; n < node_number ; n++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	create_constraints (mip, &cpt, n, mtd , m, -1, 1.0, 1.0, GLP_UP, 2, cons_numbers);
      }
      create_constraints (mip, &cpt, n , -1, m, 1, 1.0, 1.0, GLP_UP, 2, cons_numbers);
    }

    
    // cons 3 L DOBSS :
    for (int n = 0 ; n < node_number ; n++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	create_constraints (mip, &cpt, n, mtd , m, -1, 1.0, 1.0, GLP_UP, 3, cons_numbers);
      }
      create_constraints (mip, &cpt, n , -1, m, 1, 1.0, 1.0, GLP_UP, 3, cons_numbers);
    }

    // cons 3 R DOBSS :
    for (int n = 0 ; n < node_number ; n++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	create_constraints (mip, &cpt, n, mtd , m, -1, 0.0, 0.0, GLP_UP, 32, cons_numbers);
      }
      create_constraints (mip, &cpt, n , -1, m, 1, 0.0, 0.0, GLP_UP, 32, cons_numbers);
    }
    
    
    // cons 4 DOBSS :
    create_constraints (mip, &cpt, -1, -1, m, -1, 1.0, 1.0, GLP_FX, 4, cons_numbers);


    // cons 5 L Slackness :
    for (int n = 0 ; n < node_number ; n++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	create_constraints (mip, &cpt, n, mtd , m, -1, 0.0, 0.0, GLP_LO, 5, cons_numbers);
      }
      create_constraints (mip, &cpt, n , -1, m, 1, 0.0, 0.0, GLP_LO, 5, cons_numbers);
    }


    // cons 5 R Slackness :
    for (int n = 0 ; n < node_number ; n++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	create_constraints (mip, &cpt, n, mtd , m, -1, 0.0, M, GLP_UP, 52, cons_numbers);
      }
      create_constraints (mip, &cpt, n , -1, m, 1, 0.0, M, GLP_UP, 52, cons_numbers);
    }

  
    // cons 6 DOBSS : 
    if (m != 0)  {
      
      for (int n = 0 ; n < node_number ; n++) {
	for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	  create_constraints (mip, &cpt, n, mtd , m, -1, 0.0, 0.0, GLP_FX, 6, cons_numbers);
	}
	create_constraints (mip, &cpt, n , -1, m, 1, 0.0, 0.0, GLP_FX, 6, cons_numbers);
      }
    }
    
#ifdef VERBOSE
    printf("%ld\n", cpt);
#endif 
#ifdef VERBOSE
    printf("%d\n", cons_numbers[0]);
#endif 


  }
  return;
}


void Z_name_creator (char *name , int node_d, int mtd_d, int profil_p, int node_a, int mtd_a, int def_ids, int att_ids) {
  int cpt = 0;

  name[cpt++] = 'Z';

  //node number defender
  name[cpt++] = 'n';
  name[cpt++] = (node_d / 10000) % 10  + '0';
  name[cpt++] = (node_d / 1000) % 10  + '0';
  name[cpt++] = (node_d / 100) % 10  + '0';
  name[cpt++] = (node_d / 10) % 10 + '0';
  name[cpt++] = node_d % 10  + '0';
  

  if (def_ids == 1) {
    //IDS On defender node
    name[cpt++] = 'd';
  }
  else {
    // MTD number defender
    name[cpt++] = 'm';
    name[cpt++] = (mtd_d / 10000) % 10  + '0';
    name[cpt++] = (mtd_d / 1000) % 10  + '0';
    name[cpt++] = (mtd_d / 100) % 10  + '0';
    name[cpt++] = (mtd_d / 10) % 10 + '0';
    name[cpt++] = mtd_d % 10  + '0';    
  }

  //profil number
  name[cpt++] = 'p';
  name[cpt++] = (profil_p/ 10000) % 10  + '0';
  name[cpt++] = (profil_p/ 1000) % 10  + '0';
  name[cpt++] = (profil_p/ 100) % 10  + '0';
  name[cpt++] = (profil_p/ 10) % 10 + '0';
  name[cpt++] = profil_p%10  + '0';

  /* ----------- */
  
  //node number attacker
  name[cpt++] = 'n';
  name[cpt++] = (node_a / 10000) % 10  + '0';
  name[cpt++] = (node_a / 1000) % 10  + '0';
  name[cpt++] = (node_a / 100) % 10  + '0';
  name[cpt++] = (node_a / 10) % 10 + '0';
  name[cpt++] = node_a % 10  + '0';
  
  if (att_ids == 1) {
    //IDS On attacker node
    name[cpt++] = 'd';
  }
  else {
    // MTD number attacker
    name[cpt++] = 'm';
    name[cpt++] = (mtd_a / 10000) % 10  + '0';
    name[cpt++] = (mtd_a / 1000) % 10  + '0';
    name[cpt++] = (mtd_a / 100) % 10  + '0';
    name[cpt++] = (mtd_a / 10) % 10 + '0';
    name[cpt++] = mtd_a % 10  + '0';    
  }

  name[cpt++] = '\0';

#ifdef VERBOSE
  printf("node %d mtd %d profil %d node %d mtd %d ids def %d ids att %d - name : %s\n",node_d, mtd_d, profil_p, node_a, mtd_a, def_ids, att_ids, name );
#endif
  
}


void create_Znmpnm(glp_prob *mip, int node_number, int *mtd_node_number, unsigned long *cpt, int m) {
  char name[5000];

#ifdef VERBOSE
  printf("in Znmpnm\n");
#endif

  for (int node_d = 0 ; node_d < node_number ; node_d++) {
    for (int mtd_d = 0 ; mtd_d < mtd_node_number[node_d] ; mtd_d++) {
      for (int node_a = 0 ; node_a < node_number ; node_a++) {
	for (int mtd_a = 0 ; mtd_a < mtd_node_number[node_a] ; mtd_a++) {

	  Z_name_creator (name , node_d, mtd_d, m, node_a, mtd_a, 0, 0);     
	  glp_add_cols(mip, 1);
	  var_number++;
	  glp_set_col_name(mip, cpt[0], name);
	  glp_set_col_bnds(mip, cpt[0], GLP_DB, 0, 1);
	  glp_set_obj_coef(mip, cpt[0],  profile_proba[m] * (Rpnmnm[m][node_d][mtd_d][node_a][mtd_a] - Cpnmnm[m][node_d][mtd_d][node_a][mtd_a]));	  
	  glp_set_col_kind (mip, cpt[0], GLP_CV);
	  cpt[0]++;
#ifdef VERBOSE
	  printf("%s\n", name);
#endif 	
	}	
	Z_name_creator (name , node_d, mtd_d, m, node_a, -1, 0, 1);     
	glp_add_cols(mip, 1);
	var_number++;
	glp_set_col_name(mip, cpt[0], name);
	glp_set_col_bnds(mip, cpt[0], GLP_DB, 0, 1);
	glp_set_obj_coef(mip, cpt[0],  profile_proba[m] * (Rpnmnd[m][node_d][mtd_d][node_a] - Cpnmnd[m][node_d][mtd_d][node_a]));	  
	glp_set_col_kind (mip, cpt[0], GLP_CV);
	cpt[0]++;
#ifdef VERBOSE
	printf("%s\n", name);
#endif 	
      }
    }
  }
  return;
}


void create_Zndpnm(glp_prob *mip, int node_number, int *mtd_node_number, unsigned long *cpt, int m) {
  char name[5000];

#ifdef VERBOSE
  printf("*** IN Zndpnm ***\n");
#endif

    for (int node_d = 0 ; node_d < node_number ; node_d++) {
      for (int node_a = 0 ; node_a < node_number ; node_a++) {
	for (int mtd_a = 0 ; mtd_a < mtd_node_number[node_a] ; mtd_a++) {

	  Z_name_creator (name , node_d, 0, m, node_a, mtd_a, 1, 0);     
	  glp_add_cols(mip, 1);
	  var_number++;
	  glp_set_col_name(mip, cpt[0], name);
	  glp_set_col_bnds(mip, cpt[0], GLP_DB, 0, 1);
	  glp_set_obj_coef(mip, cpt[0],  profile_proba[m] * (Rpndnm[m][node_d][node_a][mtd_a] - Cpndnm[m][node_d][node_a][mtd_a]));	  
	  glp_set_col_kind (mip, cpt[0], GLP_CV);
	  cpt[0]++;
#ifdef VERBOSE
	  printf("%s , val : %g \n ", name, profile_proba[m] * (Rpndnm[m][node_d][node_a][mtd_a] - Cpndnm[m][node_d][node_a][mtd_a]));
#endif 	
	}
	Z_name_creator (name , node_d, 0, m, node_a, 0, 1, 1);     
	glp_add_cols(mip, 1);
	var_number++;
	glp_set_col_name(mip, cpt[0], name);
	glp_set_col_bnds(mip, cpt[0], GLP_DB, 0, 1);
	glp_set_obj_coef(mip, cpt[0],  profile_proba[m] * (Rpndnd[m][node_d][node_a] - Cpndnd[m][node_d][node_a]));	  
	glp_set_col_kind (mip, cpt[0], GLP_CV);
	cpt[0]++;
#ifdef VERBOSE
	printf("%s val = %g\n", name,  profile_proba[m] * (Rpndnd[m][node_d][node_a] - Cpndnd[m][node_d][node_a]));
#endif       
	
      }
    }
  return;
}



void alpha_name_creator (char *name , int profil_n, int node_n, int mtd_n, int ids) {
  int cpt = 0;
  
  name[cpt++] = 'a';
  name[cpt++] = 'l';
  name[cpt++] = 'p';
  name[cpt++] = 'h';
  name[cpt++] = 'a';
  name[cpt++] = '_';
  //profile number
  name[cpt++] = 'p';
  name[cpt++] = (profil_n / 10000) % 10  + '0';
  name[cpt++] = (profil_n / 1000) % 10  + '0';
  name[cpt++] = (profil_n / 100) % 10  + '0';
  name[cpt++] = (profil_n / 10) % 10 + '0';
  name[cpt++] = profil_n %10  + '0';
  //node number
  name[cpt++] = 'n';
  name[cpt++] = (node_n / 10000) % 10  + '0';
  name[cpt++] = (node_n / 1000) % 10  + '0';
  name[cpt++] = (node_n / 100) % 10  + '0';
  name[cpt++] = (node_n / 10) % 10 + '0';
  name[cpt++] = node_n % 10  + '0';

  if (ids == 1) {
    name[cpt++] = 'd';
  }
  else {
    // mtd number
    name[cpt++] = 'm';
    name[cpt++] = (mtd_n / 10000) % 10  + '0';
    name[cpt++] = (mtd_n / 1000) % 10  + '0';
    name[cpt++] = (mtd_n / 100) % 10  + '0';
    name[cpt++] = (mtd_n / 10) % 10 + '0';
    name[cpt++] = mtd_n % 10  + '0';
  }
  
  name[cpt] ='\0';

#ifdef VERBOSE
  printf("profil %d node %d mtd %d ids def %d - name : %s\n", profil_n, node_n, mtd_n, ids, name );
#endif

  
  return;
}


void create_alpha (glp_prob *mip, int node_number, int *mtd_node_number, unsigned long *cpt, int m) {
  char name[50000];

  // alpha bound : true
  // lb = 0 up = 1
  // Binary / integer
  // obj func coef = 0
  for (int node_n = 0 ; node_n < node_number ; node_n++) {
    for (int mtd_n = 0 ; mtd_n < mtd_node_number[node_n] ; mtd_n++) {
      alpha_name_creator (name, m, node_n, mtd_n, 0);

      glp_add_cols(mip, 1);
      var_number++;

      glp_set_col_name(mip, cpt[0], name);
      glp_set_col_bnds(mip, cpt[0], GLP_DB, 0.0, 1.0);
      glp_set_obj_coef(mip, cpt[0], 0.0);
      glp_set_col_kind (mip, cpt[0], GLP_BV);
      cpt[0]++;
#ifdef VERBOSE
      printf("%s\n", name);
#endif 
    }
    alpha_name_creator (name, m, node_n, 0, 1);

    glp_add_cols(mip, 1);
    var_number++;

    glp_set_col_name(mip, cpt[0], name);
    glp_set_col_bnds(mip, cpt[0], GLP_DB, 0.0, 1.0);
    glp_set_obj_coef(mip, cpt[0], 0.0);
    glp_set_col_kind (mip, cpt[0], GLP_BV);
    cpt[0]++;
#ifdef VERBOSE
    printf("%s\n", name);
#endif
  }
  return;
}


void A_name_creator (char *name , int m) {

  name[0] = 'p';
  if (m < 10) {
    name[1] = m + '0';
    name[2] = '_';
    name[3] = 'A';
    name[4] = '\0';    
  }
  else if (m < 100) {
    name[1] = m/10 + '0';
    name[2] = m%10 + '0';
    name[3] = '_';
    name[4] = 'A';
    name[5] = '\0';    
  }
  else if (m < 1000) {
    name[1] = m/100 + '0';
    name[2] = (m/10)%10 + '0';
    name[3] = m%10 + '0';
    name[4] = '_';
    name[5] = 'A';
    name[6] = '\0';    
  }

#ifdef VERBOSE
  printf("profil %d name : %s\n", m, name );
#endif

  
  return;
}

void create_A (glp_prob *mip, int node_number, unsigned long *cpt, int m) {
  char name[10000];

  // A bound : false
  // Continuous
  // obj func coef = 0
  A_name_creator (name ,  m);

  glp_add_cols(mip, 1);
  var_number++;
  

  glp_set_col_name(mip, cpt[0], name);
  glp_set_col_bnds(mip, cpt[0], GLP_FR, 0, 0);
  glp_set_obj_coef(mip, cpt[0], 0.0);
  glp_set_col_kind (mip, cpt[0], GLP_CV);
  cpt[0]++;
#ifdef VERBOSE
      printf("%s\n", name);
#endif 
  return;
}  

void add_variables(glp_prob *mip, int profil_number, int node_number, int *mtd_node_number ) {

  unsigned long cpt = 1;
  double k = node_number;
  
  for (int m = 0 ; m < profil_number ; m++) {
    create_Znmpnm(mip, node_number, mtd_node_number, &cpt, m);
    create_Zndpnm(mip, node_number, mtd_node_number, &cpt, m);
    if (m == 0) {
      row_znnmdp = var_number;
    }
    create_alpha (mip, node_number, mtd_node_number, &cpt, m);
    create_A (mip, node_number, &cpt, m);
    if (m == 0) {
      one_row = var_number;
    }    
  }

#ifdef VERBOSE
      printf("var number count %ld\n", cpt);
#endif 

  return;
}

void set_cons_Zn (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int m) {

  for (unsigned long i = 0 ; i < row_zij ; i++) {
    cons[ne] = consnb[0];
    var[ne] = i%var_numbers + 1 + one_row*m;
    val[ne] = 1;
    ne ++;
  }

  cpt[0] += (var_numbers * profil_number);
  consnb[0] ++;  
  return;
}


void set_cons_delta_m   (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int m, int* mtd_node_number, int node_n, int mtd_n, int node_number) {

  int shift = 0;
  
  for (int n = 0 ; n < node_n ; n ++) {
    printf("in for 1 \n");
    for (int mtd = 0 ; mtd <= mtd_n ; mtd ++) {
      printf("in for 2\n");
      for (int na = 0 ; na < node_number ; na ++) {
	printf("in for 3 \n");
	for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	  printf("in for 4 \n");
	  shift++;
	}
	shift++;
      }
      //      shift++;
    }
    //    shift++;
  }

  printf("shift : %d, node_n : %d, mtd_n : %d\n", shift, node_n, mtd_n);

  //  shift --;
  
  int i = shift;
  for (int na = 0 ; na < node_number ; na++) {
    for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda++) {
      cons[ne] = consnb[0];
      var[ne] = i%var_numbers + 1 + one_row*m;
      val[ne] = 1;
      ne ++;
      i++;
    }
    cons[ne] = consnb[0];
    var[ne] = i%var_numbers + 1 + one_row*m;
    val[ne] = 1;
    ne ++;
    i++;
  }

  
  consnb[0] ++;  
  return;
}

void set_cons_delta_d   (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int m, int* mtd_node_number, int node_number, int node_n) {

  int shift = 0;
  
  for (int n = 0 ; n < node_number ; n ++) {
    for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd ++) {
      for (int na = 0 ; na < node_number ; na ++) {
	for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	  shift++;
	}
	shift ++;
      }
    }
  }


  for (int na = 0 ; na < node_n ; na++) {
    for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda++) {
      shift++;
    }
    shift++;
  }

  
  int i = shift;


  
  for (int na = 0 ; na < node_number ; na++) {
    for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda++) {
      cons[ne] = consnb[0];
      var[ne] = i%var_numbers + 1 + one_row*m;
      val[ne] = 1;
      ne ++;
      i++;
    }
    cons[ne] = consnb[0];
    var[ne] = i%var_numbers + 1 + one_row*m;
    val[ne] = 1;
    ne ++;
    i++;
  }

  
  consnb[0] ++;  
  return;
}





void set_cons_6 (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int m, int* mtd_node_number, int node_n, int mtd_n, int node_number) {

  int shift = 0;
  
  for (int n = 0 ; n <= node_n ; n ++) {

    // add the shift for the node targeted
    if (n != 0) {
      //shift of the number of mtd on the node
      for (int mtdd = 0 ; mtdd < mtd_node_number[n-1] ; mtdd ++) {
	for (int na = 0 ; na < node_number ; na ++) {
	  for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	    shift++;
	  }
	  shift++;
	}
      }

    }
  }
    
  // add the shift depending on the mtd targeted
  for (int mtd = 0 ; mtd < mtd_n ; mtd ++) {

    for (int na = 0 ; na < node_number ; na ++) {
      for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	shift++;
      }
      shift++;
    }
  }

  

  printf("shift : %d, node_n : %d, mtd_n : %d\n", shift, node_n, mtd_n);
  
  int i = shift;
  for (int na = 0 ; na < node_number ; na++) {
    for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda++) {
      cons[ne] = consnb[0];
      var[ne] = i%var_numbers + 1 + one_row;//*m;
      val[ne] = -1;
      ne ++;
      i++;
    }
    cons[ne] = consnb[0];
    var[ne] = i%var_numbers + 1 +one_row;//*m;
    val[ne] = -1;
    ne ++;
    i++;
  }

  i = shift;
  for (int na = 0 ; na < node_number ; na++) {
    for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda++) {
      cons[ne] = consnb[0];
      var[ne] = i%var_numbers + 1;
      val[ne] = 1;
      ne ++;
      i++;
    }
    cons[ne] = consnb[0];
    var[ne] = i%var_numbers + 1;
    val[ne] = 1;
    ne ++;
    i++;
  }

  consnb[0] ++;  
  return;
}



void set_cons_62   (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int m, int* mtd_node_number, int node_number, int node_n) {

  int shift = 0;
  
  for (int n = 0 ; n < node_number ; n ++) {
    for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd ++) {
      for (int na = 0 ; na < node_number ; na ++) {
  	for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
  	  shift++;
  	}
  	shift ++;
      }
    }
  }


  for (int nd = 0 ; nd < node_n ; nd++) {
    for (int na = 0 ; na < node_number ; na ++) {
      for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	shift++;
      }
      shift++;
    }
  }


  /* for (int n = 0 ; n <= node_n ; n ++) { */

  /*   // add the shift for the node targeted */
  /*   if (n != 0) { */
  /*     //shift of the number of mtd on the node */
  /*     for (int mtdd = 0 ; mtdd < mtd_node_number[n-1] ; mtdd ++) { */
  /* 	for (int na = 0 ; na < node_number ; na ++) { */
  /* 	  for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) { */
  /* 	    shift++; */
  /* 	  } */
  /* 	  shift++; */
  /* 	} */
  /*     } */

  /*   } */
  /* } */
  
  
  int i = shift;
  for (int na = 0 ; na < node_number ; na++) {
    for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda++) {
      cons[ne] = consnb[0];
      var[ne] = i%var_numbers + 1 + one_row*m;
      val[ne] = -1;
      ne ++;
      i++;
    }
    cons[ne] = consnb[0];
    var[ne] = i%var_numbers + 1 + one_row*m;
    val[ne] = -1;
    ne ++;
    i++;
  }

  
  i = shift;
  for (int na = 0 ; na < node_number ; na++) {
    for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda++) {
      cons[ne] = consnb[0];
      var[ne] = i%var_numbers + 1;
      val[ne] = 1;
      ne ++;
      i++;
    }
    cons[ne] = consnb[0];
    var[ne] = i%var_numbers + 1 ;
    val[ne] = 1;
    ne ++;
    i++;
  }

  
  consnb[0] ++;  
  return;
}



void set_cons_Alpha (int last, unsigned long *cpt, int *consnb, unsigned long var_numbers,int node_number, int profil_number, int one_row, int row_zij, int m, int *t, int * mtd_node_number) {

  int max = 0;
  for (int n = 0 ; n < node_number ; n++) {
    // for the mtd
    max += mtd_node_number[n];
    // for the IDS
    max++;
  }
  
  // alpha
  for (unsigned long i = (var_numbers * m) + row_zij ; i < (var_numbers * m) + row_zij + max  ; i++) {
    cons[ne] = consnb[0];
    var[ne] = i%var_numbers + 1 + one_row*m;
    val[ne] = 1;
    ne ++;
    last = node_number*node_number;    
  }

  cpt[0] += (var_numbers * profil_number);
  consnb[0] ++;
  return;
}




void set_cons_3l   (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int m, int* mtd_node_number, int node_n, int mtd_n, int node_number) {

  int i = 0;
  
  for (int n = 0 ; n < node_number ; n ++) {
    for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd ++) {
      for (int na = 0 ; na < node_number ; na ++) {
	for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {

	  if ( na == node_n) {
	    if ( mtda == mtd_n) {
	      cons[ne] = consnb[0];
	      var[ne] = i%var_numbers + 1 + one_row*m;
	      val[ne] = 1;
	      ne ++;
	    }
	  }
	  i++;
	}
	i++;
      }
    }
  }


  
  for (int n = 0 ; n < node_number ; n ++) {
    for (int na = 0 ; na < node_number ; na ++) {
      for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	if ( na == node_n) {
	  if ( mtda == mtd_n) {
	    cons[ne] = consnb[0];
	    var[ne] = i%var_numbers + 1 + one_row*m;
	    val[ne] = 1;
	    ne ++;
	  }
	}
	i++;
      }
      i++;
    }
  }

  consnb[0] ++;  
  return;
}

void set_cons_32l   (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int m, int* mtd_node_number, int node_n, int mtd_n, int node_number) {

  int i = 0;

  for (int n = 0 ; n < node_number ; n ++) {
    for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd ++) {
      for (int na = 0 ; na < node_number ; na ++) {
	for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	  i++;
	}
	if (na == node_n) {
	  cons[ne] = consnb[0];
	  var[ne] = i%var_numbers + 1 + one_row*m;
	  val[ne] = 1;
	  ne ++;
	}
	i++;
      }
    }
  }


  for (int n  = 0 ; n  < node_number; n  ++) {
    for (int na = 0 ; na < node_number; na ++) {
      for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	if ( na == node_n) {
	  i++;
	}
      }
      if ( na == node_n) {
	cons[ne] = consnb[0];
	var[ne] = i%var_numbers + 1 + one_row*m;
	val[ne] = 1;
	ne ++;
      }
      i++;
    }
  }
  consnb[0] ++;  
  return;
}


void set_cons_3r   (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int m, int* mtd_node_number, int node_n, int mtd_n, int node_number) {

  int i = 0;
  
  for (int n = 0 ; n < node_number ; n ++) {
    for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd ++) {
      for (int na = 0 ; na < node_number ; na ++) {
	for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {

	  if ( na == node_n) {
	    if ( mtda == mtd_n) {
	      cons[ne] = consnb[0];
	      var[ne] = i%var_numbers + 1 + one_row*m;
	      val[ne] = -1;
	      ne ++;
	    }
	  }
	  i++;
	}
	i++;
      }
    }
  }


  for (int n = 0 ; n < node_number ; n ++) {
    for (int na = 0 ; na < node_number ; na ++) {
      for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	if ( na == node_n) {
	  if ( mtda == mtd_n) {
	    cons[ne] = consnb[0];
	    var[ne] = i%var_numbers + 1 + one_row*m;
	    val[ne] = -1;
	    ne ++;
	  }
	}
	i++;
      }
      i++;
    }
  }
  i = row_znnmdp;

  for (int na = 0 ; na < node_number ; na ++) {
    for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {

      if ( na == node_n) {
	if ( mtda == mtd_n) {
	  cons[ne] = consnb[0];
	  var[ne] = i%var_numbers + 1 + one_row*m;
	  val[ne] = 1;
	  ne ++;
	}
      }
      i++;
    }
    i++;
  }
  

  consnb[0] ++;  
  return;
}

void set_cons_32r   (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int m, int* mtd_node_number, int node_n, int mtd_n, int node_number) {

  int i = 0;

  for (int n = 0 ; n < node_number ; n ++) {
    for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd ++) {
      for (int na = 0 ; na < node_number ; na ++) {
	for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	  i++;
	}
	if (na == node_n) {
	  cons[ne] = consnb[0];
	  var[ne] = i%var_numbers + 1 + one_row*m;
	  val[ne] = -1;
	  ne ++;
	}
	i++;
      }
    }
  }


  for (int n = 0 ; n < node_number ; n ++) {
    for (int na = 0 ; na < node_number ; na ++) {
      for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	i++;
      }
      if ( na == node_n) {
	cons[ne] = consnb[0];
	var[ne] = i%var_numbers + 1 + one_row*m;
	val[ne] = -1;
	ne ++;
      }
      i++;
    }
  }


  i = row_znnmdp;

  for (int na = 0 ; na < node_number ; na ++) {
    for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
      i++;
    }
    if ( na == node_n) {
      cons[ne] = consnb[0];
      var[ne] = i%var_numbers + 1 + one_row*m;
      val[ne] = 1;
      ne ++;
    }
  
    i++;
  }
  
  
  consnb[0] ++;  
  return;
}



void set_cons_5l   (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int m, int* mtd_node_number, int node_n, int mtd_n, int node_number) {

  int i = 0;
  

  for (int n = 0 ; n < node_number ; n ++) {
    for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd ++) {
      for (int na = 0 ; na < node_number ; na ++) {
	for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	  cons[ne] = consnb[0];
	  var[ne] = i%var_numbers + 1 + one_row*m;
	  val[ne] = - (Rhpnmnm[m][n][mtd][node_n][mtd_n] - Chpnmnm[m][n][mtd][node_n][mtd_n] );
	  ne ++;
	  i++;	  
	}
	cons[ne] = consnb[0];
	var[ne] = i%var_numbers + 1 + one_row*m;
	val[ne] = - (Rhpnmnm[m][n][mtd][node_n][mtd_n] - Chpnmnm[m][n][mtd][node_n][mtd_n] );
	ne ++;
	i++;	  
      }
    }
  }


  for (int h = 0 ; h < node_number ; h ++) {
    for (int na = 0 ; na < node_number ; na ++) {
      for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	cons[ne] = consnb[0];
	var[ne] = i%var_numbers + 1 + one_row*m;
	val[ne] = - (Rhpndnm[m][h][node_n][mtd_n] - Chpndnm[m][h][node_n][mtd_n] );
	ne ++;
	i++;	  
      }
      cons[ne] = consnb[0];
      var[ne] = i%var_numbers + 1 + one_row*m;
      val[ne] = - (Rhpndnm[m][h][node_n][mtd_n] - Chpndnm[m][h][node_n][mtd_n]);
      ne ++;
      i++;	  
    }
  }
  
  cons[ne] = consnb[0];
  var[ne] = one_row + one_row*m;
  val[ne] = 1;
  ne ++;  
  
  consnb[0] ++;  
  return;
}

void set_cons_52l   (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int m, int* mtd_node_number, int node_n, int mtd_n, int node_number) {

  int i = 0;
  

  for (int n = 0 ; n < node_number ; n ++) {
    for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd ++) {
      for (int na = 0 ; na < node_number ; na ++) {
	for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	  cons[ne] = consnb[0];
	  var[ne] = i%var_numbers + 1 + one_row*m;
	  val[ne] = - (Rhpnmnd[m][n][mtd][node_n] - Chpnmnd[m][n][mtd][node_n] );
	  ne ++;
	  i++;	  
	}
	cons[ne] = consnb[0];
	var[ne] = i%var_numbers + 1 + one_row*m;
	val[ne] = - (Rhpnmnd[m][n][mtd][node_n] - Chpnmnd[m][n][mtd][node_n] );
	ne ++;
	i++;	  
      }
    }
  }


  for (int h = 0 ; h < node_number ; h ++) {
    for (int na = 0 ; na < node_number ; na ++) {
      for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	cons[ne] = consnb[0];
	var[ne] = i%var_numbers + 1 + one_row*m;
	val[ne] = - (Rhpndnd[m][h][node_n] - Chpndnd[m][h][node_n] );
	ne ++;
	i++;	  
      }
      cons[ne] = consnb[0];
      var[ne] = i%var_numbers + 1 + one_row*m;
      val[ne] = - (Rhpndnd[m][h][node_n] - Chpndnd[m][h][node_n]);
      ne ++;
      i++;	  
    }
  }
  
  cons[ne] = consnb[0];
  var[ne] = one_row + one_row*m;
  val[ne] = 1;
  ne ++;  
  
  consnb[0] ++;  
  return;
}

void set_cons_5r   (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int m, int* mtd_node_number, int node_n, int mtd_n, int node_number, double M) {

  int i = 0;
  

  for (int n = 0 ; n < node_number ; n ++) {
    for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd ++) {
      for (int na = 0 ; na < node_number ; na ++) {
	for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	  cons[ne] = consnb[0];
	  var[ne] = i%var_numbers + 1 + one_row*m;
	  val[ne] = - (Rhpnmnm[m][n][mtd][node_n][mtd_n] - Chpnmnm[m][n][mtd][node_n][mtd_n] );
	  ne ++;
	  i++;	  
	}
	cons[ne] = consnb[0];
	var[ne] = i%var_numbers + 1 + one_row*m;
	val[ne] = - (Rhpnmnm[m][n][mtd][node_n][mtd_n] - Chpnmnm[m][n][mtd][node_n][mtd_n] );
	ne ++;
	i++;	  
      }
    }
  }


  for (int na = 0 ; na < node_number ; na ++) {
    for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
      cons[ne] = consnb[0];
      var[ne] = i%var_numbers + 1 + one_row*m;
      val[ne] = - (Rhpndnm[m][na][node_n][mtd_n] - Chpndnm[m][na][node_n][mtd_n] );
      ne ++;
      i++;	  
    }
    cons[ne] = consnb[0];
    var[ne] = i%var_numbers + 1 + one_row*m;
    val[ne] = - (Rhpndnm[m][na][node_n][mtd_n] - Chpndnm[m][na][node_n][mtd_n]);
    ne ++;
    i++;	  
  }


  i = row_znnmdp;

  for (int na = 0 ; na < node_number ; na ++) {
    for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
    if ( na == node_n) {
      if ( mtda == mtd_n) {
	cons[ne] = consnb[0];
	var[ne] = i%var_numbers + 1 + one_row*m;
	val[ne] = M;
	ne ++;
      }
    }
      i++;
    }
    i++;
  }

  
  cons[ne] = consnb[0];
  var[ne] = one_row + one_row*m;
  val[ne] = 1;
  ne ++;  
  
  consnb[0] ++;  
  return;
}

void set_cons_52r   (unsigned long *cpt, int *consnb, unsigned long var_numbers, int profil_number, int one_row, int row_zij, int m, int* mtd_node_number, int node_n, int mtd_n, int node_number, double M) {

  int i = 0;
  

  for (int n = 0 ; n < node_number ; n ++) {
    for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd ++) {
      for (int na = 0 ; na < node_number ; na ++) {
	for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
	  cons[ne] = consnb[0];
	  var[ne] = i%var_numbers + 1 + one_row*m;
	  val[ne] = - (Rhpnmnd[m][n][mtd][node_n] - Chpnmnd[m][n][mtd][node_n] );
	  ne ++;
	  i++;	  
	}
	cons[ne] = consnb[0];
	var[ne] = i%var_numbers + 1 + one_row*m;
	val[ne] = - (Rhpnmnd[m][n][mtd][node_n] - Chpnmnd[m][n][mtd][node_n] );
	ne ++;
	i++;	  
      }
    }
  }


  for (int na = 0 ; na < node_number ; na ++) {
    for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
      cons[ne] = consnb[0];
      var[ne] = i%var_numbers + 1 + one_row*m;
      val[ne] = - (Rhpndnd[m][na][node_n] - Chpndnd[m][na][node_n] );
      ne ++;
      i++;	  
    }
    cons[ne] = consnb[0];
    var[ne] = i%var_numbers + 1 + one_row*m;
    val[ne] = - (Rhpndnd[m][na][node_n] - Chpndnd[m][na][node_n]);
    ne ++;
    i++;	  
  }


  i = row_znnmdp;

  for (int na = 0 ; na < node_number ; na ++) {
    for (int mtda = 0 ; mtda < mtd_node_number[na] ; mtda ++) {
      i++;
    }
    if ( na == node_n) {
      cons[ne] = consnb[0];
      var[ne] = i%var_numbers + 1 + one_row*m;
      val[ne] = M;
      ne ++;
    }  
    i++;
  }


  
  cons[ne] = consnb[0];
  var[ne] = one_row + one_row*m;
  val[ne] = 1;
  ne ++;  
  
  consnb[0] ++;  
  return;
}




void set_constraints (glp_prob *mip, unsigned long var_numbers, int node_number, int *mtd_node_number, int profil_number, int one_row, int row_zij, double k, double M, int cons_numbers, int mtd_max) {
  
  long int last = 0;
  unsigned long cpt = 1;
  int consnb = 1;
  int t = 0;

  long int min = 0, max = 0;

  for (int m = 0 ; m < profil_number ; m++) {
    min = 0;
    max = 0;

    //for (int n = 0 ; n < node_number ; n++) {
    // constraints 1 DOBSS :
    printf ("set constraint 1\n");
    set_cons_Zn (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m);
    //}
      
    // constraints 2 DOBSS :
    for (int n = 0 ; n < node_number ; n++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	set_cons_delta_m (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m, mtd_node_number, n, mtd, node_number );
      }
      set_cons_delta_d (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m, mtd_node_number, node_number, n );
    }


    // constraints 3 L DOBSS :
    for (int n = 0 ; n < node_number ; n++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	set_cons_3l (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m, mtd_node_number, n, mtd, node_number );
      }
      set_cons_32l (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m, mtd_node_number, n, 0, node_number );
    }

    // constraints 3 R DOBSS :
    for (int n = 0 ; n < node_number ; n++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	set_cons_3r (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m, mtd_node_number, n, mtd, node_number );
      }
      set_cons_32r (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m, mtd_node_number, n, 0,node_number );
    }

        
    // constraints 4 DOBSS :
    set_cons_Alpha (last, &cpt, &consnb, var_numbers, node_number, profil_number, one_row, row_zij, m, &t, mtd_node_number);    


    // constraints 5 L DOBSS :
    for (int n = 0 ; n < node_number ; n++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	set_cons_5l (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m, mtd_node_number, n, mtd, node_number );
      }
      set_cons_52l (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m, mtd_node_number, n, 0, node_number );
    }

    // constraints 5 R DOBSS :
    for (int n = 0 ; n < node_number ; n++) {
      for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	set_cons_5r (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m, mtd_node_number, n, mtd, node_number, M );
      }
      set_cons_52r (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m, mtd_node_number, n, 0,node_number, M );
    }

    

    // constraints 6 DOBSS:
    if (m != 0) {
      for (int n = 0 ; n < node_number ; n++) {
	for (int mtd = 0 ; mtd < mtd_node_number[n] ; mtd++) {
	  set_cons_6 (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m, mtd_node_number, n, mtd, node_number );
	}
	set_cons_62 (&cpt, &consnb, var_numbers, profil_number, one_row, row_zij, m, mtd_node_number, node_number, n );
      }
    }

    
  }


   printf ("cpt : %ld, matrix \n", cpt);
   printf ("ne : %d, element != 0 \n", ne);  
   glp_load_matrix(mip, ne-1, cons, var, val);
   printf ("cpt = %ld \n", cpt  );  
  return;
}


void fill_constant(FILE *fp, int *node_number, int mtd_node_number[] ,int *profil_number, double *M, char pb_name[255], double profil_proba[1000]) {

  char buff[455];
  unsigned long cpt = 0;
  int o;

  // Fetch the problem name
  while (fgets(buff, 255, fp) != NULL) {
    // '#' = commentary in the configuration file
    if (buff[0] != '#') {
      strcpy (pb_name, buff);
      pb_name[strlen(pb_name)-1] = '\0';
      break;
    }
  }
  
  // Fill the constants
  while (fscanf(fp, "%s", buff) != EOF) {
    // '#' = commentary in the configuration file
    if (buff[0] != '#') {
      if (cpt == 0) {
	*profil_number = atof(buff);
	cpt ++;
#ifdef VERBOSE
	printf ("profil_number : %d \n", *profil_number);
#endif
      }
      else if (cpt == 1) {
      	*node_number = atof(buff);
      	cpt ++;
#ifdef VERBOSE
	printf ("node_number : %d \n", *node_number);
#endif
      }
      else if (cpt < 2 + *node_number) {
      	mtd_node_number[cpt-2] = atof(buff);
      	cpt ++;
#ifdef VERBOSE
      	printf ("mtd_node_number : %d \n", mtd_node_number[cpt-3]);
#endif
      }
      else if (cpt == 2 + *node_number) {
      	*M = atof(buff);
#ifdef VERBOSE
      	printf ("M : %f \n", *M);
#endif
      	cpt++;
      	o=0;
      }
      else if (cpt > 2 + *node_number) {
	profil_proba[o] = atof(buff);
#ifdef VERBOSE
      	printf ("profil_proba[%d] : %f \n", o, profil_proba[o]);
#endif
      	o++;
      	if (o == *profil_number){
      	  break;
	}
      }
    }
    else {
      fgets(buff, 255, fp); 
	}
  }
  return;
}

void fill_reward_rnmpnm (FILE *fp, int *mtd_node_number, int node_number, int profil_number, double R[SIZE][SIZE][SIZE_MTD][SIZE][SIZE_MTD],char*name) {
  char buff[255];
  unsigned long cpt = 0;
  int profil_n, node_d, mtd_d, node_a, mtd_a;

  // Fill the Rijs tab
  while (fscanf(fp, "%s", buff) != EOF) {
    // '#' = commentary in the configuration file    
    if (buff[0] != '#') {
      if (strcmp (buff, "STOP") == 0) {
      	return;
      }      
      if (cpt == 0) {
      	profil_n = atof (buff);
      	cpt ++;
      }
      else if (cpt == 1) {
      	node_d = atof (buff);
      	cpt ++;
      }
      else if (cpt == 2) {
      	mtd_d = atof (buff);
      	cpt ++;
      }
      else if (cpt == 3) {
      	node_a = atof (buff);
      	cpt ++;
      }
      else if (cpt == 4) {
      	mtd_a = atof (buff);
      	cpt ++;
      }

      else {
	R[profil_n][node_d][mtd_d][node_a][mtd_a] = atof(buff);
#ifdef VERBOSE
      	printf ("%s[%d][%d][%d][%d][%d] = %f \n", name, profil_n, node_d, mtd_d, node_a, mtd_a, R[profil_n][node_d][mtd_d][node_a][mtd_a]);
#endif
	cpt = 0;
      }
    }
    else {
      fgets(buff, 255, fp); 
    }
} 
  return;
}

void fill_reward_rnmpnd (FILE *fp, int *mtd_node_number, int node_number, int profil_number, double R[SIZE][SIZE][SIZE_MTD][SIZE],char*name) {
  char buff[255];
  unsigned long cpt = 0;
  int profil_n, node_d, mtd_d, node_a;

  // Fill the Rijs tab
  while (fscanf(fp, "%s", buff) != EOF) {
    // '#' = commentary in the configuration file    
    if (buff[0] != '#') {
      if (strcmp (buff, "STOP") == 0) {
      	return;
      }      
      if (cpt == 0) {
      	profil_n = atof (buff);
      	cpt ++;
      }
      else if (cpt == 1) {
      	node_d = atof (buff);
      	cpt ++;
      }
      else if (cpt == 2) {
      	mtd_d = atof (buff);
      	cpt ++;
      }
      else if (cpt == 3) {
      	node_a = atof (buff);
      	cpt ++;
      }
      else {
	R[profil_n][node_d][mtd_d][node_a] = atof(buff);
#ifdef VERBOSE
      	printf ("%s[%d][%d][%d][%d] = %f \n", name, profil_n, node_d, mtd_d, node_a, R[profil_n][node_d][mtd_d][node_a]);
#endif
	cpt = 0;
      }
    }
    else {
      fgets(buff, 255, fp); 
    }
} 
  return;
}


void fill_reward_rndpnm (FILE *fp, int *mtd_node_number, int node_number, int profil_number, double R[SIZE][SIZE][SIZE][SIZE_MTD],char*name) {
  char buff[255];
  unsigned long cpt = 0;
  int profil_n, node_d, node_a, mtd_a;

  // Fill the Rijs tab
  while (fscanf(fp, "%s", buff) != EOF) {
    // '#' = commentary in the configuration file    
    if (buff[0] != '#') {
      if (strcmp (buff, "STOP") == 0) {
      	return;
      }      
      if (cpt == 0) {
      	profil_n = atof (buff);
      	cpt ++;
      }
      else if (cpt == 1) {
      	node_d = atof (buff);
      	cpt ++;
      }
      else if (cpt == 2) {
      	node_a = atof (buff);
      	cpt ++;
      }
      else if (cpt == 3) {
      	mtd_a = atof (buff);
      	cpt ++;
      }

      else {
	R[profil_n][node_d][node_a][mtd_a] = atof(buff);
#ifdef VERBOSE
      	printf ("%s[%d][%d][%d][%d] = %f \n", name, profil_n, node_d, node_a, mtd_a, R[profil_n][node_d][node_a][mtd_a]);
#endif
	cpt = 0;
      }
    }
    else {
      fgets(buff, 255, fp); 
    }
} 
  return;
}


void fill_reward_rndpnd (FILE *fp, int node_number, int profil_number, double R[SIZE][SIZE][SIZE],char*name) {
  char buff[255];
  unsigned long cpt = 0;
  int profil_n, node_d, node_a;
  
  // Fill the Rijs tab
  while (fscanf(fp, "%s", buff) != EOF) {
    // '#' = commentary in the configuration file    
    if (buff[0] != '#') {
      if (strcmp (buff, "STOP") == 0) {
      	return;
      }      
      if (cpt == 0) {
      	profil_n = atof (buff);
      	cpt ++;
      }
      else if (cpt == 1) {
      	node_d = atof (buff);
      	cpt ++;
      }
      else if (cpt == 2) {
      	node_a = atof (buff);
      	cpt ++;
      }

      else {
	R[profil_n][node_d][node_a] = atof(buff);
#ifdef VERBOSE
      	printf ("%s[%d][%d][%d] = %f \n", name, profil_n, node_d, node_a, R[profil_n][node_d][node_a]);
#endif
	cpt = 0;
      }
    }
    else {
      fgets(buff, 255, fp); 
    }
} 
  return;
}


void fill_matrices(FILE *fp, int node_number, int mtd_node_number[],int profil_number, double M ) {

  printf ("\n");

  //For the Defender
  fill_reward_rnmpnm (fp, mtd_node_number, node_number, profil_number, Rpnmnm, "Rpnmnm");
  fill_reward_rnmpnd (fp, mtd_node_number, node_number, profil_number, Rpnmnd, "Rpnmnd");
  fill_reward_rndpnm (fp, mtd_node_number, node_number, profil_number, Rpndnm, "Rpndnm");
  fill_reward_rndpnd (fp, node_number, profil_number, Rpndnd, "Rpndnd");
  
  fill_reward_rnmpnm (fp, mtd_node_number, node_number, profil_number, Cpnmnm, "Cpnmnm");
  fill_reward_rnmpnd (fp, mtd_node_number, node_number, profil_number, Cpnmnd, "Cpnmnd");
  fill_reward_rndpnm (fp, mtd_node_number, node_number, profil_number, Cpndnm, "Cpndnm");
  fill_reward_rndpnd (fp, node_number, profil_number, Cpndnd, "Cpndnd");

  //For the Attacker
  fill_reward_rnmpnm (fp, mtd_node_number, node_number, profil_number, Rhpnmnm, "Rhpnmnm");
  fill_reward_rnmpnd (fp, mtd_node_number, node_number, profil_number, Rhpnmnd, "Rhpnmnd");
  fill_reward_rndpnm (fp, mtd_node_number, node_number, profil_number, Rhpndnm, "Rhpndnm");
  fill_reward_rndpnd (fp, node_number, profil_number, Rhpndnd, "Rhpndnd");
  
  fill_reward_rnmpnm (fp, mtd_node_number, node_number, profil_number, Chpnmnm, "Chpnmnm");
  fill_reward_rnmpnd (fp, mtd_node_number, node_number, profil_number, Chpnmnd, "Chpnmnd");
  fill_reward_rndpnm (fp, mtd_node_number, node_number, profil_number, Chpndnm, "Chpndnm");
  fill_reward_rndpnd (fp, node_number, profil_number, Chpndnd, "Chpndnd");


  printf ("\n"); 
  return;
}


int main(int argc, char* argv[])
{  
  if (argc != 3) { 
    printf ("Wrong usage, 1 arg required :\n ./solver.x \"configuration file\" \"g\" for GLPK usage or \"c\" for CLEPX usage \n");  
    return -1; 
  } 

  FILE *fp; 
  int node_number; 
  int mtd_node_number[550];
  int mtd_max;
  int profil_number;
  double M;
  char pb_name[255];



  val  = malloc(1000000000 *sizeof(double));
  cons = malloc(1000000000 *sizeof(double));
  var  = malloc(1000000000 *sizeof(double));

  fp = fopen(argv[1], "r"); 
  if (fp == NULL) { 
    printf ("file %s doesn'n exist \n", argv[1]);  
    return -1;     
  } 

  
  fill_constant(fp, &node_number, mtd_node_number, &profil_number, &M, pb_name, profile_proba);  
  for (int i = 0 ; i < node_number ; i++ ) {
    mtd_max +=  mtd_node_number[i];
  }
  fill_matrices(fp, node_number, mtd_node_number, profil_number, M);

  double k = node_number;
      
  int cons_numbers =  0; 
  
  /* Create the Problem with the name extract from the file 
     As a Maximization problem
   */
  glp_prob *mip = glp_create_prob();
  glp_set_prob_name (mip, pb_name);
  glp_set_obj_dir (mip, GLP_MAX);

  
  /* Add the constraints to the problem  */
  add_constraints (mip, &cons_numbers, profil_number, node_number, M, row_znnmdp, mtd_max, mtd_node_number);
  
  /* Add the variables to the problem  */
  add_variables(mip, profil_number, node_number, mtd_node_number);

#ifdef VERBOSE
  printf("var_number : %ld, one_row : %d, row_znnmdp %d \n", var_number, one_row, row_znnmdp);
#endif 

  
  /* Add the constraints to the problem  */
  set_constraints (mip, var_number, node_number, mtd_node_number, profil_number, one_row, row_znnmdp, k,  M, cons_numbers, mtd_max);
  
#ifdef VERBOSE
  printf ("**Problem name : %s **\n\n", glp_get_prob_name(mip));  
  printf ("**The objective function direction (1:MIN;2:MAX) : %d **\n", glp_get_obj_dir(mip));
  printf ("\nThe constraints : \n");
  for (int i = 1 ; i <= cons_numbers ; i++) {
     printf("%s lb : %g; ub : %g\n", glp_get_row_name(mip, i),  glp_get_row_lb(mip, i), glp_get_row_ub(mip, i));
   }
#endif

  int ind[10000];
  double val[10000];
  
  int len;
  int c = 0;

#ifdef VERBOSE
  printf ("\n**The constraints coeffs values: **\n");
  for (int i = 1 ; i <= cons_numbers ; i++) {

    printf("%s : ", glp_get_row_name(mip, i));
    len = glp_get_mat_row(mip, i, ind, val);
   
    for (int j = 1 ; j <= var_number; j++) {
      c = 0;
      for (int l = 1 ; l <= len ; l ++) {
	if (ind[l] == j) {
	  printf("%g*%s + ", val[l], glp_get_col_name(mip, j));
	  c = 1;
	}
      }
      if (c == 0 ) {
	printf("%g*%s + ", 0.0, glp_get_col_name(mip, j));
      }
      if (j%one_row == 0) {
	printf("\n       ");
      }
      else if (j%node_number == 0) {
	printf("\n       ");
      }
    }
    printf("\n");
  }
  printf("\n");



     printf ("\n**The objective function definition : **\n");
   for (int i = 1 ; i <= var_number ; i++) {
     printf("%g*%s + ", glp_get_obj_coef(mip, i),  glp_get_col_name(mip, i));
     if ((i % one_row) == 0) {
       printf("\n");
     }
     if ((i % node_number) == 0) {
       printf("\n");
     }
   }
   printf("\n");
#endif  
  
  /* Solve the problem  */
  glp_iocp parm;
  glp_init_iocp(&parm);
  parm.presolve = GLP_ON;

  // Check if using CPLEX and return
  if (argv[2][0] == 'c') {
    char result_file[300];
    sprintf (result_file, "./test.lp");//, argv[1]);
    glp_write_lp(mip, NULL, result_file);
    return 0;
  }

  // Or using GLPK and continue
  glp_intopt(mip, &parm);
  printf("\n** The result**\n");

  double output[2000];
  for (int i = 0 ; i < one_row ; i++) {

    output[i] = 0;


  }

  
  int g = 0;
  int pf = 0;
  int count = 0;
  int count_n = 0;
  int sov_g = 0;
  
  for (int i = 1 ; i <= profil_number; i++) {
    for (int j = 1 ; j <= one_row ; j++) {
      if (j%node_number == 0) {
	printf (" %s = %g\n", glp_get_col_name(mip, (((i-1)*one_row)+j)), (glp_mip_col_val(mip, (((i-1)*one_row)+j))));
      }
      else {
	printf (" %s = %g -  ", glp_get_col_name(mip, (((i-1)*one_row)+j)), (glp_mip_col_val(mip, (((i-1)*one_row)+j))));
      }
    }
    printf("\n");
  }

  unsigned long cptt = 0;
  unsigned long cptt2 = 0;
  int max = 0;
   
  for (int i = 0 ; i < node_number ; i++) {
    for (int j = 0 ; j < mtd_node_number[i] ; j++) {
      printf ("\\delta_{%d%d} =  %.4f - ", i+1, j+1,  output[cptt]);
      cptt++;
    }
    
    printf ("\\delta_{%dd} =  %.4f\\\\\n", i+1,  output[cptt2+(max)]);
    cptt2++;
  }

  int t = 1;
  for (int i = 1 ; i <= var_number; i++) {

    if ((i % one_row) > row_znnmdp) {
      printf("\\alpha_{%d%d} = %.2f - ", t, (i%one_row)-row_znnmdp, glp_mip_col_val(mip,i));
    }
    if ((i % one_row) == 0) {
      t++;
      printf ("\n");
    }
  }
   
  printf("\n");


   return 0;
}

