#!/bin/bash

if [ $# -ne 1 ]
then
    echo "need a scenario in arg"
    exit 1
fi

mkdir -p ./results
mkdir -p ./results/$1

make all

./bin/solver_convert.x  $1 c >> ./results/$1/trace

echo "read "$1"_test.lp" > ./results/$1/res.cplex
echo "opt" >> ./results/$1/res.cplex
echo "write ./results/"$1"/res.sol" >> ./results/$1/res.cplex
echo "y" >> ./results/$1/res.cplex
echo "quit" >> ./results/$1/res.cplex

cplex -f ./results/$1/res.cplex

echo "Trace write in ./results/"$1"/trace"
echo "solution write in ./results/"$1"/res.sol -"

rm cplex.log
