#!/bin/bash

if [ $# -ne 1 ]
then
    echo "need number max to compute time "
    exit 1
fi


mkdir -p generated_sc
mkdir -p results
mkdir -p bin

make all


for i in $(seq 1 $1)
do
    
    mkdir -p generated_sc/scenario$i
    rm -f generated_sc/scenario$i/dump

    for j in $(seq 1 5)
    do

    name=scenario$i\_$j
	
	./bin/parameters_generator_node.x $i $i generated_sc/scenario$i/param_$name
	./bin/parameters_to_rewards_and_costs.x generated_sc/scenario$i/param_$name  generated_sc/scenario$i/$name

	{ time timeout 6000 ./bin/solver_convert.x generated_sc/scenario$i/$name g >> generated_sc/scenario$i/trace_glpk  ; } 2>> generated_sc/scenario$i/dump
	
	sleep 1
    done

done

./bin/plot_glpk.x $1
