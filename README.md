# Install

make clean depend all

## Needed Tools :

- GCC (https://gcc.gnu.org/)
- GLPK (https://www.gnu.org/software/glpk/)
- CPLEX (https://www.ibm.com/analytics/cplex-optimizer)
- GNUPLOT (http://www.gnuplot.info/)


# Repository construction 

- README.md : This file.
- Makefile : The Makefile to compile the source code of this project. Work with $make clean depend all command
- src/ : The directory with all the source code of this project.
- tests/ : The directory with all tests scenarios to use with the tool.
- *.sh : The scripts to use the tools.
- bin/ : Containing the executables of this project, appears after using the Makefile.
- generated_sc/ : Containing the generated scenario by the tools, appears after a scenario is generated.
- results/ : Containing the final results generated by the tools, appears after some results are produced.

# Usage 

- To compute a solution for a specific scenario Sc with CPLEX or GLPK:
$./compute_scenario_cplex(/glpk).sh Path_To_A_Scenario

- To compute a solution for X attacker profiles - Y nodes with CPLEX or GLPK:
$./compute_random_scenario_cplex(/glpk).sh X Y 

- To compute the nedded time to find a solution by CPLEX or GLPK for 1 to X node/attacker profiles
$./compute_time_cplex(/glpk).sh X

- To generate a random scenario with name Nm for X attacker profiles and Y nodes
$./generate_scenario.sh X Y nm

# Question - Bug report

name.lastname@telecom-paris.fr

