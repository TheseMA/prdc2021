#!/bin/bash

mkdir -p test_node_cplex
make all

for i in {355..500}
do
    mkdir -p test_node_cplex/scenario$i
    touch test_node_cplex/scenario$i/dump
    
    for j in 1 2 3 4 
    do
	./parameters_generator_node.x $i $i test_node_cplex/scenario$i/param_scenario$j
	sleep 1
	./parameters_to_rewards_and_costs.x  test_node_cplex/scenario$i/param_scenario$j test_node_cplex/scenario$i/scenario$j

	./solver_convert_fix_cplex.x test_node_cplex/scenario$i/scenario$j

	cplex -f cmd.cplex >>  test_node_cplex/scenario$i/dump 	
    done
done
