#!/bin/bash

if [ $# -ne 2 ]
then
    echo "need number of attacker profile and node "
    exit 1
fi

name=scenario$1\_$2_cplex

mkdir -p generated_sc
mkdir -p generated_sc/$name
mkdir -p results
mkdir -p bin

make all

	
./bin/parameters_generator_node.x $1 $2 generated_sc/$name/param_$name
./bin/parameters_to_rewards_and_costs.x generated_sc/$name/param_$name  generated_sc/$name/$name

./bin/solver_convert.x generated_sc/$name/$name c >> generated_sc/$name/trace

echo "read ./generated_sc/"$name"/"$name"_test.lp" > ./generated_sc/$name/res.cplex
echo "opt" >> ./generated_sc/$name/res.cplex
echo "write ./results/"$name"_res.sol" >> ./generated_sc/$name/res.cplex
echo "quit" >> ./generated_sc/$name/res.cplex

cplex -f ./generated_sc/$name/res.cplex

echo "Trace write in ./results/generated_sc/"$name"/trace"
echo "solution write in ./results/generated_sc/"$name"/res.sol -"

rm -f cplex.log
