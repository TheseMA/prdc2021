#!/bin/bash

if [ $# -ne 2 ]
then
    echo "need number of attacker profile and node "
    exit 1
fi

name=scenario$1\_$2

mkdir -p generated_sc
mkdir -p generated_sc/$name
mkdir -p results
mkdir -p bin

make all

	
./bin/parameters_generator_node.x $1 $2 generated_sc/scenario$1\_$2/param_$name
./bin/parameters_to_rewards_and_costs.x generated_sc/scenario$1\_$2/param_$name  generated_sc/scenario$1\_$2/$name

{ time timeout 6000 ./bin/solver_convert.x generated_sc/scenario$1\_$2/$name g > results/$name\_res_glpk  ; } 2>> generated_sc/scenario$1\_$2/dump

echo "solution write in ./results/generated_sc/"$name"/res_glpk.sol -"

