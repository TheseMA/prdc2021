#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, char* argv[])
{  
  if (argc != 2) { 
    printf ("Wrong usage, 1 arg required :\n ./plot_solver_results.x \" nb profile\"  \n");  
    return -1; 
  } 

  // The number of attacker profiles
  int profile_number = atoi(argv[1]);
  
  FILE *fp; 
  fp = fopen("results/plot.plt", "w");   
  if (fp == NULL) { 
    printf ("error opening file %s \n", "results/plot_cplex.plt");  
    return -1;     
  }
  fprintf(fp,"set terminal pngcairo \nset output  'plot_cplex.png'\nset xlabel 'nb profiles'\nset ylabel 'time to compute in s'\nset grid\nplot 'data_cplex.plt' using 1:2  title 'time taken to compute the solution - timeout : 10min'");
  fclose(fp);


  fp = fopen("results/data_cplex.plt", "w"); 
  if (fp == NULL) { 
    printf ("error opening file %s \n", "results/plot.plt");  
    return -1;     
  }
  fprintf(fp, "# Profile number - used MTD\n");  

  FILE *fg;
  char name[40] =  "generated_sc/scenario1/dump\0";  
  char buff[255];

  fprintf(fp, "0 0\n");      
  for (int i = 1 ; i <= profile_number ; i++) {
    if (i < 10) {    
      name[21] = i + '0' ;
      printf("%s\n",name);
    }
    else if (i < 100) {    
      name[21] = i/10 + '0' ;
      name[22] = i%10 + '0' ;
      sprintf(name+23, "%s", "/dump\0");
      printf("%s\n",name);
    }
    else {
      name[21] = i/100 + '0' ;
      name[22] = (i-((i/100)*100) )/10 + '0' ;
      name[23] = i%10 + '0' ;
      sprintf(name+24, "%s", "/dump\0");
      printf("%s\n",name);
    }

    fg = fopen(name, "r"); 
    if (fg == NULL) { 
      printf ("error opening dump file for scenario_%d \n", i);  
      return -1;     
    }

    while (fscanf(fg, "%s", buff) != EOF) {
      if (strcmp (buff, "Total") == 0) {
	// read (root and branch)
	fscanf(fg, "%s", buff);
	// read =
	fscanf(fg, "%s", buff);
	//read sec
	fscanf(fg, "%s", buff);
	//Total (root+branch&cut) =    0.31 sec. (19.28 ticks)

	fprintf(fp, "%d %s\n", i, buff); 
      }
    }	
    fclose(fg);
}
  
  fprintf(fp, "%d 0\n", profile_number);   
  fclose(fp);
  return 0;
}
